#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  2 11:39:57 2021

@author: mothikaa
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import math

# Reading the data from csv to Dataframe

dataset = pd.read_csv("/home/mothikaa/Desktop/ML/house_prices.csv")
size = dataset['sqft_living15']
price = dataset['price']

print(size)


# ML can handle only arrays not dataframes
x = np.array(size).reshape(-1,1)
y = np.array(size).reshape(-1,1)

# We are using Linear Regression here

model = LinearRegression()
model.fit(x,y) # fit is the training module


regression_model_mse = mean_squared_error(x,y)
print("MSE: ",math.sqrt(regression_model_mse))
print("R Squared Value: ", model.score(x, y))

# Now we can get the b values after the model fit
# this is the b0
print(model.coef_[0])
# this is the b1 in our model
print(model.intercept_[0])


# Now visualise the dataset with the fitted model

plt.scatter(x, y, color='green')
plt.plot(x, model.predict(x), color='black')
plt.title("Linear Regression")
plt.xlabel("Size")
plt.ylabel("Price")
plt.show()

# Predicting the prices

print("Prediction by the model :", model.predict([[2000]]))









